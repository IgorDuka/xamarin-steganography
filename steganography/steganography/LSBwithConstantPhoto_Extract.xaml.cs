﻿using Android.Graphics;
using Newtonsoft.Json;
using Plugin.Media;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace steganography
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LSBwithConstantPhoto : ContentPage
	{
		public LSBwithConstantPhoto ()
		{
			InitializeComponent ();
            _ImageSource.IsVisible = false;
        }
        public string _ImageSource_Path;
        public string _ImagewithHidenData_Path;
        public byte[] Bytes_ImageSource;
        public byte[] Bytes_ImagewithHidenData;
        public Data_LSB_with_photo_library Data;

        private async void ImageSource_Clicked(object sender, EventArgs args)
        {
            _ImageSource.IsVisible = false;
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Full,
            });

            if (file == null)
                return;

            _ImageSource_Path = file.Path;
            
            //Картинка образец
            byte[] la1 = File.ReadAllBytes(_ImageSource_Path);
            Bitmap Bitmap_ImageSource = BitmapFactory.DecodeByteArray(la1, 0, la1.Length).Copy(Bitmap.Config.Argb8888, true);
            LSB_with_photo_library lsb = new LSB_with_photo_library();

            Bytes_ImageSource = lsb.CutImage(Bitmap_ImageSource, Data.CountPixels);
            Bitmap_ImageSource.Dispose();
            await DisplayAlert("!!!", "Изображение было добавленно", "OK!");
        }
        private async void ImagewithHidenData_Clicked(object sender, EventArgs args)
        {
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
            {
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Full,
            });

            if (file == null)
                return;

            _ImagewithHidenData_Path = file.Path;

            //Установить картинку
            _ImagewithHidenData.Source = ImageSource.FromStream(() =>
            {
                var stream = file.GetStream();
                _ImagewithHidenData_Path = file.Path;
                file.Dispose();
                return stream;
            });

            //Картинка со скрытой информацией
            byte[] la2 = File.ReadAllBytes(_ImagewithHidenData_Path);
            Bitmap Bitmap_ImagewithHidenData = BitmapFactory.DecodeByteArray(la2, 0, la2.Length).Copy(Bitmap.Config.Argb8888, true);
            LSB_with_photo_library lsb = new LSB_with_photo_library();

            int countPixels = lsb.ExtractBitesHidenMessage(Bitmap_ImagewithHidenData);
            bool Invert = lsb.ExtractBitInvert(Bitmap_ImagewithHidenData);

            Data = new Data_LSB_with_photo_library(Invert, countPixels);

            Bytes_ImagewithHidenData = lsb.CutImage(Bitmap_ImagewithHidenData, countPixels);
            _ImageSource.IsVisible = true;
            Bitmap_ImagewithHidenData.Dispose();
            await DisplayAlert("!!!", "Изображение было добавленно", "OK!");
        }
        private void Extract_Click(object sender, EventArgs args)
        {
            try
            {
                var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                var filename = System.IO.Path.Combine(documents, "DefaultData.json");
                var readData = File.ReadAllText(filename);
                SavedData appData = JsonConvert.DeserializeObject<SavedData>(readData);

                AES aes = new AES(Methods.Out16Rich(appData.KeyAES), Methods.Out16Rich(appData.IVAES));

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();

                LSB_with_photo_library With_lib = new LSB_with_photo_library(Math.Abs(Convert.ToInt32(appData.Salt)),
                    Bytes_ImageSource, Bytes_ImagewithHidenData);

                Text_Dec.Text = Encoding.Unicode.GetString(aes.Decrypt(With_lib.Extract(Data)));
                //Text_Dec.Text = Encoding.Unicode.GetString(With_lib.Extract(Data));

                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                DisplayAlert("!!!", "Процесс успешно завершен!\n При этом потребовалось: " + elapsedTime, "OK!");
            }
            catch
            {
                DisplayAlert("!!!", "В настройка выберите подходящий ключ шифрования и вектор " +
                    "инициализации, после чего следует сохранить настройки!", "OK!");
            }
        }
        private async void Copy_text_Click(object sender, EventArgs args)
        {
            try
            {
                await Clipboard.SetTextAsync(Text_Dec.Text);
                DisplayAlert("!!!", "Текст успешно скопирован в буфер обмена!", "OK!");
            }
            catch
            {
                DisplayAlert("!!!", "Произошла ошибка копирования текста!", "OK!");
            }
        }

    }
}