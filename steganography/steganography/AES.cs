﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace steganography
{
    public class AES
    {
        public AES()
        {
            using (Aes aesSecrets = Aes.Create())
            {
                aesSecrets.GenerateKey();
                aesSecrets.GenerateIV();
                Key = aesSecrets.Key;
                IV = aesSecrets.IV;
            }
        }
        public AES(byte[] Key, byte[] IV)
        {
            this.Key = Key;
            this.IV = IV;
        }
        public byte[] Key { get; private set; }
        public byte[] IV { get; private set; }

        private byte[] PerformCryptography(byte[] data, ICryptoTransform cryptoTransform)
        {
            using (var ms = new MemoryStream())
            using (var cryptoStream = new CryptoStream(ms, cryptoTransform, CryptoStreamMode.Write))
            {
                cryptoStream.Write(data, 0, data.Length);
                cryptoStream.FlushFinalBlock();

                return ms.ToArray();
            }
        }

        public byte[] Encrypt (byte[] text)
        {
            using (var aes = new AesManaged())
            {
                aes.Key = Key;
                aes.IV = IV;
                var encryptor = aes.CreateEncryptor(aes.Key, aes.IV);
                using (var encryptedStream = new MemoryStream())
                {
                    using (var cryptoStream = new CryptoStream(encryptedStream, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(text, 0, text.Length);
                    }
                    return encryptedStream.ToArray();
                }
            }
        }
        public byte[] Decrypt(byte[] text)
        {
            using (var aes = new AesManaged())
            {
                aes.Key = Key;
                aes.IV = IV;
                var decryptor = aes.CreateDecryptor(aes.Key, aes.IV);
                using (var dataStream = new MemoryStream(text))
                {
                    using (var cryptoStream = new CryptoStream(dataStream, decryptor, CryptoStreamMode.Read))
                    {
                        var decrypted = new byte[text.Length];
                        var bytesRead = cryptoStream.Read(decrypted, 0, text.Length);

                        return decrypted.Take(bytesRead).ToArray();
                    }
                }
            }
        }
    }
}
