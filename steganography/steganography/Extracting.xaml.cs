﻿using Android.Graphics;
using Newtonsoft.Json;
using Plugin.Media;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace steganography
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Declassify : ContentPage//ContentView
    {
		public Declassify()
		{
			InitializeComponent();
		}

        public string resource;
        private string ChosenAlgorithm = "";

        private async void Work_Image_Clicked(object sender, EventArgs e){
            if (!CrossMedia.Current.IsPickPhotoSupported){
                await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions{
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Full,
            });

            if (file == null)
                return;

            Work_Image.Source = ImageSource.FromStream(() => {
                var stream = file.GetStream();
                resource = file.Path;
                file.Dispose();
                return stream;
            });
        }

        void SelectedItemsAlgorithm(object sender, EventArgs e){
            ChosenAlgorithm = Picker_algorithms.Items[Picker_algorithms.SelectedIndex];
            Picker_algorithms.Title = "Chosen: " + ChosenAlgorithm;
            DisplayAlert("!!!", "Вы выбрали: " + ChosenAlgorithm, "OK!");
        }

        private void Decode_Click(object sender, EventArgs args)
        {
            //byte[] text = Encoding.Unicode.GetBytes("Да, в коде трудно ориентироваться, несмотря на то, что он небольшой. Abstract" +
            //    " – Was carried out the complex analysis of the most relevant data hiding algorithms by multi-objective optimization.");
            //    AES aes = new AES();
            //byte[] enc = aes.Encrypt(text);
            //Text_decoded.Text = Encoding.Unicode.GetString(aes.Decrypt(enc));

            if (ChosenAlgorithm != "")
            {
                byte[] la = File.ReadAllBytes(resource);
                Bitmap ima = BitmapFactory.DecodeByteArray(la, 0, la.Length);
                Bitmap mutable = ima.Copy(Bitmap.Config.Argb8888, true);

                var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                var filename = System.IO.Path.Combine(documents, "DefaultData.json");
                var readData = File.ReadAllText(filename);
                SavedData appData = JsonConvert.DeserializeObject<SavedData>(readData);

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                switch (ChosenAlgorithm)
                {
                    case "LSB":
                        AES aes = new AES(Methods.Out16Rich(appData.KeyAES), Methods.Out16Rich(appData.IVAES));
                        LSB lsb = new LSB(mutable);
                        byte[] data = lsb.ExtractData();
                        Text_decoded.Text = Encoding.Unicode.GetString(aes.Decrypt(data));
                        //Text_decoded.Text = Encoding.Unicode.GetString(data);
                        break;
                    case "Koch-Zhao":
                        Koch_Zhao koch = new Koch_Zhao(mutable);
                        byte[] text = Coder.Decoding(koch.Extract(mutable));
                        Text_decoded.Text = Encoding.Unicode.GetString(text);
                        break;
                }
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                DisplayAlert("!!!", "Процесс успешно завершен!\n При этом потребовалось: " + elapsedTime, "OK!");
            }
            else
                DisplayAlert("!!!", "Выберите алгоритм!", "OK!");
        }

        private async void Copy_text_Click(object sender, EventArgs args)
        {
            try
            {
                await Clipboard.SetTextAsync(Text_decoded.Text);
                await DisplayAlert("!!!", "Текст успешно скопирован в буфер обмена!", "OK!");
            }
            catch
            {
                await DisplayAlert("!!!", "Произошла ошибка копирования текста!", "OK!");
            }
        }

    }
}