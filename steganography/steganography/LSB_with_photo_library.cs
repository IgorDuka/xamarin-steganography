﻿using Android.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace steganography
{
    public class Data_LSB_with_photo_library
    {
        public bool Invert { get; set; }
        public int CountPixels { get; set; }
        public Data_LSB_with_photo_library(bool Invert, int CountPixels)
        {
            this.Invert = Invert;
            this.CountPixels = CountPixels;
        }
    }
    public class LSB_with_photo_library
    {
        public byte[] Text { get; set; }
        public int Seed { get; set; }
        private Bitmap SourceImage { get; set; }
        private byte[] Byte_SourceImage { get; set; }
        private byte[] Byte_ImageWithHIdenText { get; set; }
        public byte[] RandomArray { get; private set; }
        public string Path { get; private set; }

        public LSB_with_photo_library() { }
        public LSB_with_photo_library(byte[] text, int seed, Bitmap SourceImage, string Path)
        {
            Text = text;
            Seed = seed;
            this.SourceImage = SourceImage;
            this.Path = Path;

            Randomazer rnd = new Randomazer();
            rnd.GetArrayOne(Seed, Text.Length * 8);
            RandomArray = rnd.Array;
        }
        public LSB_with_photo_library(int Seed, byte[] SourceImage, byte[] ImageWithHIdenText)
        {
            this.Seed = Seed;
            this.Byte_SourceImage = SourceImage;
            this.Byte_ImageWithHIdenText = ImageWithHIdenText;
        }

        public void Hide()
        {
            byte[] secret = Text;
            byte[] secret_8 = CutByteOnbit(secret);
            if (MoreOne(secret_8))//1 More
            {
                SourceImage = InsertBitInvert(SourceImage, true);
                secret_8 = Invert(secret_8);
            }
            else SourceImage = InsertBitInvert(SourceImage, false);
            
            // * в последнии 4 бита картинки нужно вставить количество сгенериных псевдо случайных чисел
            SourceImage = InsertBitesHidenMessage(RandomArray.Length, SourceImage);
            SourceImage = HideMessage(SourceImage, RandomArray, secret_8);

            Save(Path, SourceImage);
        }
        public byte[] Extract(Data_LSB_with_photo_library Data)
        {
            Randomazer rnd = new Randomazer();
            rnd.GetArray(Seed, Data.CountPixels);

            byte[] text = ExtractMessage(Byte_SourceImage, Byte_ImageWithHIdenText, rnd.Array);

            if (Data.Invert)
                text = Invert(text);

            text = Add8bitIn1(text);

            return text;
        }
        /// <summary>
        /// Метод извлекает количество пикселей из картинки
        /// </summary>
        /// <param name="Image"></param>
        /// <param name="countPixels"></param>
        /// <returns></returns>
        public byte[] CutImage(Bitmap Image, int countPixels)
        {
            int counter = 0;
            byte[] arrayBlueBits = new byte[countPixels];
            for(int y = 0; y < Image.Height; y++)
                for(int x = 0; x < Image.Width; x++, counter++)
                {
                    if (counter < countPixels)
                        arrayBlueBits[counter] = Convert.ToByte(Color.GetBlueComponent(Image.GetPixel(x, y)));
                    else return arrayBlueBits;
                }
            return null;
        }
        /// <summary>
        /// Разбиение 8-битового значение на 8 ячеек по 1 символу в том же порядке
        /// </summary>
        /// <param name="Array"></param>
        /// <returns></returns>
        private byte[] CutByteOnbit(byte[] Array)
        {
            byte[] OneArray = new byte[Array.Length * 8];
            for (int i = 0, symbols = 0; i < (Array.Length * 8); i += 8, symbols++)
            {
                for (int j = 0, shift = 7; j < 8; j++, shift--)
                    OneArray[i + j] = (byte)((Array[symbols] >> shift) & 0x0000_0001);
            }
            return OneArray;
        }
        /// <summary>
        /// byte-вый массив по одному символу собрать в массив по 8 символов
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        private byte[] Add8bitIn1(byte[] array)
        {
            byte[] result = new byte[array.Length / 8];
            for (int i = 0, counter = 0; counter < result.Length; i += 8, counter++)
                result[counter] = (byte)((array[i] << 7)
                                    | (array[i + 1] << 6)
                                    | (array[i + 2] << 5)
                                    | (array[i + 3] << 4)
                                    | (array[i + 4] << 3)
                                    | (array[i + 5] << 2)
                                    | (array[i + 6] << 1)
                                    | (array[i + 7] << 0));
            return result;
        }
        /// <summary>
        /// Если единиц больше или равно количеству 0 то тру, иначе фолс
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public bool MoreOne(byte[] vector)
        {
            int count1 = 0, count0 = 0;
            for (int i = 0; i < vector.Length; i++)
                if (vector[i] == 0x1)
                    count1++;
                else if (vector[i] == 0x0)
                    count0++;
            if (count0 > count1)
                return false;
            return true;
        }
        /// <summary>
        /// Invert array
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public byte[] Invert(byte[] vector)
        {
            for (int i = 0; i < vector.Length; i++)
                vector[i] = (vector[i] == 0x1) ? (byte)(0x0) : (byte)(0x1);
            return vector;
        }
        /// <summary>
        /// Валидный ли текст
        /// </summary>
        /// <param name="imageLength"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public bool ValidText(int image, int random)
        {
            if (image > random)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Сокрытие в картинке данных
        /// </summary>
        /// <param name="Image">Картинка исходная</param>
        /// <param name="Random">Массив псевдослучайных битов формата: [0000_0001/0000_0000]</param>
        /// <param name="Message">Массив битов сообщения формата: [0000_0001/0000_0000]</param>
        /// <returns></returns>
        private Bitmap HideMessage(Bitmap Image, byte[] Random, byte[] Message)
        {
            int counterForRandom = 0;
            int counterForMessage = 0;
            for (int i = 0; i < Image.Height; i++)
            {
                for (int j = 0; j < Image.Width; j++, counterForRandom++)
                {
                    if (Random[counterForRandom] == 0x1)
                    {
                        if (counterForMessage < Message.Length)
                        {
                            int pixel = Image.GetPixel(j, i);
                            Image.SetPixel(j, i, new Color(
                                Convert.ToByte(Color.GetRedComponent(pixel)),
                                Convert.ToByte(Color.GetGreenComponent(pixel)),
                                Convert.ToByte(Color.GetBlueComponent(pixel)) ^ Message[counterForMessage]));
                            counterForMessage++;
                        }
                        else
                        {
                            return Image;
                        }
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Извлечение битов в скрытой картинке
        /// </summary>
        /// <param name="SourceImage"></param>
        /// <param name="ImageWithText"></param>
        /// <param name="Random"></param>
        /// <returns></returns>
        private byte[] ExtractMessage(byte[] SourceImage, byte[] ImageWithText, byte[] Random)
        {
            List<byte> Message = new List<byte>();
            for(int i = 0; i < Random.Length; i++)
                if (Random[i] == 0x1)
                    Message.Add((byte)(SourceImage[i] ^ ImageWithText[i]));
            return Message.ToArray();
        }
        /// <summary>
        /// Вставка в последнии биты количества сгенериных псевдо случайных чисел для определения скрытого текста
        /// </summary>
        /// <returns></returns>
        private Bitmap InsertBitesHidenMessage(int length, Bitmap Image)
        {
            byte[] array = BitConverter.GetBytes(length);
            for (int i = 0, back = 4; i < 4; i++, back--)
            {
                int pixel = Image.GetPixel(Image.Width - back, Image.Height - 1);
                Color color = new Color(
                            Convert.ToByte(Color.GetRedComponent(pixel)),
                            Convert.ToByte(Color.GetGreenComponent(pixel)),
                            array[i]);
                Image.SetPixel(Image.Width - back, Image.Height - 1, color);
            }

            return Image;
        }
        /// <summary>
        /// вставить бит инверсирования
        /// </summary>
        /// <param name="Image"></param>
        /// <param name="Invert"></param>
        /// <returns></returns>
        private Bitmap InsertBitInvert(Bitmap Image, bool Invert)
        {
            Color color = new Color();
            int pixel = Image.GetPixel(Image.Width - 5, Image.Height - 1);
            if (Invert)//Invert
                color = new Color(
                            Color.GetRedComponent(pixel),
                            Color.GetGreenComponent(pixel),
                            255);
            else if (!Invert)//No invert
                color = new Color(
                            Color.GetRedComponent(pixel),
                            Color.GetGreenComponent(pixel),
                            0);
            Image.SetPixel(Image.Width - 5, Image.Height - 1, color);
            return Image;
        }
        /// <summary>
        /// Извлечь бит инверсирования
        /// </summary>
        /// <returns></returns>
        public bool ExtractBitInvert(Bitmap Image)
        {
            int pixel = Image.GetPixel(Image.Width - 5, Image.Height - 1);
            byte Invert = Convert.ToByte(Color.GetBlueComponent(pixel));
            if (Invert == 255)
                return true;
            else if (Invert == 0)
                return false;
            return false;
        }
        /// <summary>
        /// Извлечь последнии биты количества сгенериных псевдо случайных чисел для определения скрытого текста
        /// </summary>
        /// <returns></returns>
        public int ExtractBitesHidenMessage(Bitmap Image)
        {
            byte[] length = new byte[4];
            for (int i = 0, back = 4; i < 4; i++, back--)
            {
                int pixel = Image.GetPixel(Image.Width - back, Image.Height - 1);
                length[i] = Convert.ToByte(Color.GetBlueComponent(pixel));
            }
            return BitConverter.ToInt32(length, 0);
        }
        public class Randomazer
        {
            private Random rn;
            public byte[] Array;
            /// <summary>
            /// Возврат псевдослучайного массива с определенным к-вом единиц
            /// </summary>
            /// <param name="seed"></param>
            /// <param name="countElementsOne"></param>
            public void GetArrayOne(int seed, int countElementsOne)
            {
                rn = new Random(seed);
                List<byte> Array = new List<byte>();
                int counter = 0;

                for (int i = 0; ; i++)
                {
                    if (counter <= countElementsOne + 1)
                    {
                        Array.Add(Convert.ToByte(rn.Next(0, 2)));
                        if (Array[i] == 0x1)
                            counter++;
                    }
                    else
                        break;
                }
                this.Array = Array.ToArray();
            }
            /// <summary>
            /// Возврат массива псевдослучайных чисел исходя из к-ва необходимых символов
            /// </summary>
            /// <param name="seed"></param>
            /// <param name="countElementsOne"></param>
            public void GetArray(int seed, int countElements)
            {
                rn = new Random(seed);
                byte[] Array = new byte[countElements];

                for (int i = 0; i < countElements; i++)
                {
                    Array[i] = Convert.ToByte(rn.Next(0, 2));
                }
                this.Array = Array;
            }
        }
        /// <summary>
        /// Save Image
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="Image"></param>
        public void Save(string Path, Bitmap Image)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            string s = Path.Remove(0, Path.LastIndexOf("/") + 1);
            var filePath = System.IO.Path.Combine(sdCardPath, s);
            var stream = new FileStream(filePath, FileMode.Create);
            Image.Compress(Bitmap.CompressFormat.Png, 100, stream);
            stream.Close();
        }
    }
}