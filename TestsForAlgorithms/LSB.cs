﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestsForAlgorithms
{
    class LSB
    {
        /// <summary>
        /// Картинка на которой происходит работа прямой или обратной операции
        /// </summary>
        public Bitmap Image { get; private set; }
        /// <summary>
        /// Текст который прячеться или извлекаеться
        /// </summary>
        public byte[] Text { get; private set; }
        /// <summary>
        /// Путь для сохранения файла
        /// </summary>
        public string Directory { get; private set; }
        /// <summary>
        /// For Hide
        /// </summary>
        /// <param name="Image">Image</param>
        /// <param name="Text">Hiden text</param>
        /// <param name="Directory">directory for resultaly image</param>
        public LSB(Bitmap Image, byte[] Text, string Directory)
        {
            this.Image = Image;
            this.Text = Text;
            this.Directory = Directory;
        }
        /// <summary>
        /// For extract
        /// </summary>
        /// <param name="Image">Image</param>
        public LSB(Bitmap Image)
        {
            this.Image = Image;
        }
        /// <summary>
        /// Hiden data in image
        /// </summary>
        public Bitmap HideLSB()
        {
            byte[] secret = Text;
            //LSB
            int countPixels = secret.Length * 8;
            byte[] imageArr = GetBitsImage(Image, countPixels);
            byte[] messageOnes = CutByteOnbit(secret);// Делаем новый массив исходного сообщения по одному байту
            //imageArr = PastHidenMessageInImage(messageOnes, imageArr);
            Image = PastBytesInImage(Image, messageOnes, imageArr, messageOnes.Length);
            Image = InsertCountPixelsWithData(countPixels, Image);
            return Image;
        }
        /// <summary>
        /// Extract data from image
        /// </summary>
        /// <returns></returns>
        public byte[] ExtractData()
        {
            //extract
            int countPixels = ExtractCountPixelsWithData(Image);
            byte[] imageArr = GetBitsImage(Image, countPixels);
            imageArr = GetLSB(imageArr);//массив найменее значущих битов с изображения
            return Add8bitIn1(imageArr);
        }

        /// <summary>
        /// Разбиение 8-битового значение на 8 ячеек по 1 символу в том же порядке
        /// </summary>
        /// <param name="Array"></param>
        /// <returns></returns>
        private byte[] CutByteOnbit(byte[] Array)
        {
            byte[] OneArray = new byte[Array.Length * 8];
            for (int i = 0, symbols = 0; i < (Array.Length * 8); i += 8, symbols++)
            {
                for (int j = 0, shift = 7; j < 8; j++, shift--)
                    OneArray[i + j] = (byte)((Array[symbols] >> shift) & 0x0000_0001);
            }
            return OneArray;
        }
        /// <summary>
        /// Из картинки извлечь найменне значущие биты
        /// </summary>
        /// <param name="Image"></param>
        /// <returns></returns>
        private byte[] GetLSB(byte[] ByteFromImageBlue)
        {
            byte[] res = new byte[ByteFromImageBlue.Length];
            for (int i = 0; i < ByteFromImageBlue.Length; i++)
                res[i] = (byte)(ByteFromImageBlue[i] & 0x0000_0001);
            return res;
        }
        /// <summary>
        /// Извлечь из картинки голубые биты
        /// </summary>
        /// <param name="Image"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private byte[] GetBitsImage(Bitmap Image, int count)
        {
            byte[] ByteFromImageBlue = new byte[count];
            for (int i = 0, index = 1; i < Image.Height; i++)
            {
                for (int j = 0; j < Image.Width; j++, index++)
                {
                    if (index <= count)
                    {
                        ByteFromImageBlue[i * Image.Width + j] = (byte)(Convert.ToByte(Image.GetPixel(j, i).B));
                    }
                    else return ByteFromImageBlue;
                }
            }
            return ByteFromImageBlue;
        }
        /// <summary>
        /// Сокрытие данных в картинке
        /// </summary>
        /// <param name="Image">картинка</param>
        /// <param name="message">массив битов сообщения</param>
        /// <param name="BluePixels">массив синих пекселей картинки</param>
        /// <param name="length">к-во витов сообщения</param>
        /// <returns></returns>
        private Bitmap PastBytesInImage(Bitmap Image, byte[] message, byte[] BluePixels, int length)
        {
            for (int i = 0; i < length; i++)
            {
                Color color = Color.FromArgb(Image.GetPixel((i % Image.Width), (i / Image.Width)).R,
                    Image.GetPixel((i % Image.Width), (i / Image.Width)).G,
                    ((byte)(((BluePixels[i] >> 1) << 1) | message[i])));
                Image.SetPixel((i % Image.Width), (i / Image.Width), color);
            }
            return Image;
        }
        /// <summary>
        /// byte-вый массив по одному символу собрать в массив по 8 символов
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        private byte[] Add8bitIn1(byte[] array)
        {
            byte[] result = new byte[array.Length / 8];
            for (int i = 0, counter = 0; counter < result.Length; i += 8, counter++)
                result[counter] = (byte)((array[i] << 7)
                                    | (array[i + 1] << 6)
                                    | (array[i + 2] << 5)
                                    | (array[i + 3] << 4)
                                    | (array[i + 4] << 3)
                                    | (array[i + 5] << 2)
                                    | (array[i + 6] << 1)
                                    | (array[i + 7] << 0));
            return result;
        }

        /// <summary>
        /// Определение помещаеться ли текст в изображение
        /// </summary>
        /// <param name="text"></param>
        /// <param name="Image"></param>
        /// <returns></returns>
        public bool GetVerdict(string text, Bitmap Image)
        {
            int countBitText = (Encoding.Unicode.GetBytes(text).Length * 8) + (3 * 8);
            int countPixels = Image.Height * Image.Width;
            if (countBitText < countPixels)
                return true;//текст помещаеться
            return false;//текст не помещаеться
        }

        /// <summary>
        /// Метод извлекает количество пикселей из картинки
        /// </summary>
        /// <returns></returns>
        public int ExtractCountPixelsWithData(Bitmap Image)
        {
            byte[] length = new byte[4];
            for (int i = 0, back = 4; i < 4; i++, back--)
            {
                length[i] = Convert.ToByte(Image.GetPixel(Image.Width - back, Image.Height - 1).B);
            }
            return BitConverter.ToInt32(length, 0);
        }
        /// <summary>
        /// Вставка в последнии биты количества скрытого текста
        /// </summary>
        /// <returns></returns>
        public Bitmap InsertCountPixelsWithData(int length, Bitmap Image)
        {
            byte[] array = BitConverter.GetBytes(length);
            for (int i = 0, back = 4; i < 4; i++, back--)
            {
                Color color = Color.FromArgb(
                            Image.GetPixel(Image.Width - back, Image.Height - 1).R,
                            Image.GetPixel(Image.Width - back, Image.Height - 1).G,
                            array[i]);
                Image.SetPixel(Image.Width - back, Image.Height - 1, color);
            }
            return Image;
        }
    }
}
