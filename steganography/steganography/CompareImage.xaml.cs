﻿using Android.Graphics;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;




namespace steganography
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CompareImage : ContentPage
    {
        public CompareImage()
        {
            InitializeComponent();
        }

        string resource1, resource2;

        private async void SetImage(ImageButton button, string LeftOrRight){
            if (!CrossMedia.Current.IsPickPhotoSupported){
                await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions{
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Full,
            });

            if (file == null)
                return;

            button.Source = ImageSource.FromStream(() => {
                var stream = file.GetStream();
                if(LeftOrRight == "Left") resource1 = file.Path;
                if (LeftOrRight == "Right") resource2 = file.Path;
                file.Dispose();
                return stream;
            });
        }

        private void SetImageLeft(object sender, EventArgs e){
            SetImage(ImageButton_Left, "Left");
        }

        private void SetImageRight(object sender, EventArgs e){
            SetImage(ImageButton_Right, "Right");
        }

        private void Button_Compare(object sender, EventArgs e){
            byte[] arrImage1 = File.ReadAllBytes(resource1);
            Bitmap buf1 = BitmapFactory.DecodeByteArray(arrImage1, 0, arrImage1.Length);
            Bitmap Image1 = buf1.Copy(Bitmap.Config.Argb8888, true);

            byte[] arrImage2 = File.ReadAllBytes(resource2);
            Bitmap buf2 = BitmapFactory.DecodeByteArray(arrImage2, 0, arrImage2.Length);
            Bitmap Image2 = buf2.Copy(Bitmap.Config.Argb8888, true);

            if (EqualSizes(Image1, Image2))
                SaveImage(CompareImages(Image1, Image2, PB_compare), resource1);
            else DisplayAlert("!!!", "Images not equal size!", "OK");
            DisplayAlert("!!!", "Count - " + counter, "Ok!");
            return;
        }
        private static int counter;

        public static Bitmap CompareImages(Bitmap image1, Bitmap image2, Xamarin.Forms.ProgressBar ProgBar)
        {
             counter = 0;//33333333333333
            ProgBar.Progress = (1 / (image1.Height * image1.Width));
            for (int i = 0; i < image1.Width; i++)
            {
                for (int j = 0; j < image1.Height; j++)
                {
                    ProgBar.ProgressTo(1, (uint)(1 / (image1.Height * image1.Width)), Xamarin.Forms.Easing.Linear);//for form
                    if (image1.GetPixel(i, j) == image2.GetPixel(i, j))
                    {
                        int color = image1.GetPixel(i, j);
                        int R = Android.Graphics.Color.GetRedComponent(color) + 190 < 255 ? Android.Graphics.Color.GetRedComponent(color) + 190 : 255;
                        int G = Android.Graphics.Color.GetGreenComponent(color) + 190 < 255 ? Android.Graphics.Color.GetGreenComponent(color) + 190 : 255;
                        int B = Android.Graphics.Color.GetBlueComponent(color) + 190 < 255 ? Android.Graphics.Color.GetBlueComponent(color) + 190 : 255;
                        image1.SetPixel(i, j, Android.Graphics.Color.Rgb(R, G, B));
                    }
                    else { 
                        image1.SetPixel(i, j, Android.Graphics.Color.Rgb(255, 0, 0));
                        counter++;//3333333333333
                    }
                }
            }
            
            return image1;
        }

        public static void SaveImage(Bitmap image, string directory)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            var filePath = System.IO.Path.Combine(sdCardPath, "CompareImages.png");
            var stream = new FileStream(filePath, FileMode.Create);
            image.Compress(Bitmap.CompressFormat.Png, 100, stream);
            stream.Close();
        }

        public static bool EqualSizes(Bitmap image1, Bitmap image2)
        {
            if ((image1.Width == image2.Width) && (image1.Height == image2.Height))
                return true;
            else return false;
        }

    }
}