﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace steganography
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async private void Embed_Click(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new Concealment());
        }

        async private void Extract_Click(object sender, EventArgs e)
        {
            var action = await DisplayActionSheet("Выбрать:", "Отмена", "ОК", "LSB, Коха-Жао", "LSB с постоянными картинками");
            if (action == "LSB с постоянными картинками")
                await Navigation.PushModalAsync(new LSBwithConstantPhoto());
            else if (action == "LSB, Коха-Жао")
                await Navigation.PushModalAsync(new Declassify());
        }

        async private void Settings_Click(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new Settings());
        }

        async private void Help_Click(object sender, EventArgs e)
        {
            await Browser.OpenAsync("https://github.com", BrowserLaunchMode.SystemPreferred);
        }

        //async private void CompareImages_Click(object sender, EventArgs e)
        //{
        //    await Navigation.PushModalAsync(new CompareImage());
        //    //Navigation.RemovePage( new MainPage());
        //}

        private void PhonesList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }
    }
}
