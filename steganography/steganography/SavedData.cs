﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Internals;

namespace steganography
{
    [Preserve]
    public class SavedData
    {
        public SavedData(string KeyAES, string IVAES, string Salt)
        {
            this.KeyAES = KeyAES;
            this.IVAES = IVAES;
            this.Salt = Salt;
        }
        /// <summary>
        /// Ключ шифрования AES
        /// </summary>
        public string KeyAES { get; set; }
        /// <summary>
        /// Вектор инициализации AES
        /// </summary>
        public string IVAES { get; set; }
        /// <summary>
        /// Соль для генерации псевдослучайных последовательностей
        /// </summary>
        public string Salt { get; set; }
    }
}
