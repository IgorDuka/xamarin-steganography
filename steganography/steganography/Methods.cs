﻿using Android.Graphics;
using Java.IO;
using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;

namespace steganography
{
    public class Methods
    {
        public static byte[] Out16Rich(string a)            //Перевод 16-ричного вида в поток байтов
        {
            string[] substrings = a.Split('-');
            byte[] masiv = new byte[substrings.Length];
            for (int i = 0; i < substrings.Length; i++)
                masiv[i] = Convert.ToByte(substrings[i], 16);
            return masiv;
        }
        public static string Creat16Rich(byte[] array)
        {
            return BitConverter.ToString(array);
        }

        
    }


    /*
         HideSeek 
Цей алгоритм випадковим чином розподіляє повідомлення по зображеніню. Він використовує пароль для генерації випадкового seed, а потім використовує цей 
seed, щоб вибрати першу позицію для приховування. Він продовжує випадковим чином генерувати позиції, поки не закінчить приховувати 
повідомлення. Це трохи розумніші про те, як він ховається, тому що ви повинні спробувати кожну комбінацію пікселів в кожному порядку, 
щоб спробувати "зламати" алгоритм - якщо у вас немає пароля. Це все ще не кращий метод, тому що він не дивиться на пікселі, в яких він 
ховається, - було б більш корисно з'ясувати області зображення, де краще ховатися.
*/
    public class HideSeek
    {
        private int Width;
        private int Height;
        private Bitmap Image;
        private string Text;
        private string directory;

        public HideSeek(Bitmap image, string text, string directory)
        {
            Width = image.Width;
            Height = image.Height;
            Image = image;
            Text = text;
            this.directory = directory;
        }

        public HideSeek(Bitmap image)
        {
            Width = image.Width;
            Height = image.Height;
            Image = image;
        }

        //private byte[] ConvertBitmapToByteArray_Png(Bitmap bitmap)
        //{
        //    byte[] BitmapArr = new byte[bitmap.Width * bitmap.Height * 4];
        //    MemoryStream stream = new MemoryStream(BitmapArr);
        //    bitmap.Compress(Bitmap.CompressFormat.Png, 80, stream);
        //    stream.Flush();
        //    return BitmapArr;
        //}

        //private byte[] ConvertBitmapToByteArray_Jpeg(Bitmap bitmap)
        //{
        //    byte[] BitmapArr = new byte[bitmap.Width * bitmap.Height * 4];
        //    MemoryStream stream = new MemoryStream(BitmapArr);
        //    bitmap.Compress(Bitmap.CompressFormat.Jpeg, 80, stream);
        //    stream.Flush();
        //    return BitmapArr;
        //}

        private int GeneratePassword()
        {
            int pass = DateTime.Now.Millisecond;
            return pass;
        }

        private int[,] GenerateSeed(int pass, int lengText)
        {
            bool[,] arr = new bool[Width, Height];
            int[,] Seed = new int[2, lengText];

            for (int i = 0; i < Width; i++)
                for (int j = 0; j < Height; j++)
                    arr[i, j] = true;

            Random rnd = new Random(pass);

            for (int i = 0; i < lengText; i++)
            {
                do
                {
                    Seed[0, i] = rnd.Next(0, Width);
                    Seed[1, i] = rnd.Next(0, Height);
                }
                while (arr[Seed[0, i], Seed[1, i]] == false);
                arr[Seed[0, i], Seed[1, i]] = false;
            }
            return Seed;
        }

        private void SaveImage(Bitmap image, int pass, string directory)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;

            string s = directory.Remove(0, directory.LastIndexOf("/") + 1);

            var filePath = System.IO.Path.Combine(sdCardPath, s);
            var stream = new FileStream(filePath, FileMode.Create);
            image.Compress(Bitmap.CompressFormat.Png, 100, stream);
            stream.Close();
        }

        public Bitmap Steganography()
        {//принимать переменую которая отвечае в какой цвет вставлять сообшение, default blue
            int password = GeneratePassword();
            int[,] seed = GenerateSeed(password, Text.Length);

            for (int i = 0; i < Text.Length; i++)
            {
                int pixel = Image.GetPixel(seed[0, i], seed[1, i]);
                int R = Android.Graphics.Color.GetRedComponent(pixel);
                int G = Android.Graphics.Color.GetGreenComponent(pixel);
                Android.Graphics.Color color = new Android.Graphics.Color(R, G, Convert.ToByte(Text[i]));
                Image.SetPixel(seed[0, i], seed[1, i], color);
            }

            Image = InportLengthTextFromImage(Image, Text);

            SaveImage(Image, password, directory);
            return Image;
        }

        private Bitmap InportLengthTextFromImage(Bitmap image, string text)
        {


            return image;
        }

        private int ExportLengthTextFromImage(Bitmap image)//другой тип данных который меньше места занимает
        {
            int length = 0;



            return length;
        }

        private int GetPasswordFromImage()
        {
            int pass = 0;
            //
            //continue code...
            //
            return pass;
        }

        public string DeSteganography()
        {
            int password = GetPasswordFromImage();
            int length = ExportLengthTextFromImage(Image);
            int[,] seed = GenerateSeed(password, length);
            string TextFromImage = "";

            for (int i = 0; i < length; i++)
            {
                int pixel = Image.GetPixel(seed[0, i], seed[1, i]);
                TextFromImage += Android.Graphics.Color.GetBlueComponent(pixel);
            }

            return TextFromImage;
        }


    }

    public static class Compare
    {
        public static Bitmap CompareImages(Bitmap image1, Bitmap image2, Xamarin.Forms.ProgressBar ProgBar)
        {
            ProgBar.ProgressTo(1 / (image1.Height * image1.Width), 1, Xamarin.Forms.Easing.Linear);//for form
            ProgBar.Progress = 0;
            for (int i = 0; i < image1.Width; i++)
            {
                for (int j = 0; j < image1.Height; j++)
                {
                    ProgBar.Progress++;
                    if (image1.GetPixel(i, j) == image2.GetPixel(i, j))
                    {
                        int color = image1.GetPixel(i, j);
                        int R = Color.GetRedComponent(color) + 100 < 255 ? Color.GetRedComponent(color) + 100 : 255;
                        int G = Color.GetGreenComponent(color) + 100 < 255 ? Color.GetGreenComponent(color) + 100 : 255;
                        int B = Color.GetBlueComponent(color) + 100 < 255 ? Color.GetBlueComponent(color) + 100 : 255;
                        image1.SetPixel(i, j, Color.Rgb(R, G, B));
                    }
                    else
                        image1.SetPixel(i, j, Color.Rgb(255, 0, 0));
                }
            }
            return image1;
        }

        public static void SaveImage(Bitmap image, string directory)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            var filePath = System.IO.Path.Combine(sdCardPath, "CompareImages.png");
            var stream = new FileStream(filePath, FileMode.Create);
            image.Compress(Bitmap.CompressFormat.Png, 100, stream);
            stream.Close();
        }

        public static bool EqualSizes(Bitmap image1, Bitmap image2)
        {
            if ((image1.Width == image2.Width) && (image1.Height == image2.Height))
                return true;
            else return false;
        }
    }
}
