﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using steganography;
using System.IO;
using System.Text;
using System.Drawing;
using System;

namespace TestsForAlgorithms
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAES()
        {
            string Text = "Возможность нахождения поисковых ключей в тексте и определения их количества полезна как для написания нового текста, так и для оптимизации уже существующего. Расположение ключевых слов по группам и по " +
                          "частоте сделает навигацию по ключам удобной и быстрой. Сервис также найдет и морфологические вашианты ключей, которые выделятся в тексте при нажатии на нужное ключевое слово. Определение процента водности " +
                          "текста When answering a question please: Read the question carefully. Understand that English isn't everyone's first language so be lenient of bad spelling and grammar. If a question is poorly phrased then " +
                          "either ask for clarification, ignore it, or edit the question and fix the problem.Insults are not welcome. Dont tell someone to read the manual. Chances are they have and dont get it.Provide an answer or move " +
                          "on to the next question.";
            AES aes = new AES();
            byte[] byteText = Encoding.Unicode.GetBytes(Text);
            byte[] Encrypted = aes.Encrypt(byteText);
            byte[] Decrypted = aes.Decrypt(Encrypted);
            string newTest = Encoding.Unicode.GetString(Decrypted);
            Assert.AreEqual(Text, newTest);
        }
        [TestMethod]
        public void TestCoding()
        {
            string Text = "Возможность нахождения поисковых ключей в тексте и определения их количества полезна как для написания нового текста, так и для оптимизации уже существующего. Расположение ключевых слов по группам и по " +
                          "частоте сделает навигацию по ключам удобной и быстрой. Сервис также найдет и морфологические вашианты ключей, которые выделятся в тексте при нажатии на нужное ключевое слово. Определение процента водности " +
                          "текста When answering a question please: Read the question carefully. Understand that English isn't everyone's first language so be lenient of bad spelling and grammar. If a question is poorly phrased then " +
                          "either ask for clarification, ignore it, or edit the question and fix the problem.Insults are not welcome. Dont tell someone to read the manual. Chances are they have and dont get it.Provide an answer or move " +
                          "on to the next question.";
            byte[] byteText = Encoding.Unicode.GetBytes(Text);
            byte[] coded = Coder.Coding(byteText);
            byte[] decoded = Coder.Decoding(coded);
            string newTest = Encoding.Unicode.GetString(decoded);
            Assert.AreEqual(Text, newTest);
        }
        [TestMethod]
        public void TestLSB()
        {
            //Hide
            string StringText = "Возможность нахождения поисковых ключей в тексте и определения их количества полезна как для написания нового текста, так и для оптимизации уже существующего. Расположение ключевых слов по группам и по " +
                          "частоте сделает навигацию по ключам удобной и быстрой. Сервис также найдет и морфологические вашианты ключей, которые выделятся в тексте при нажатии на нужное ключевое слово. Определение процента водности " +
                          "текста When answering a question please: Read the question carefully. Understand that English isn't everyone's first language so be lenient of bad spelling and grammar. If a question is poorly phrased then " +
                          "either ask for clarification, ignore it, or edit the question and fix the problem.Insults are not welcome. Dont tell someone to read the manual. Chances are they have and dont get it.Provide an answer or move " +
                          "on to the next question.";
            string Path = @"C:\Users\Mr. Igor\source\repos\steganography\TestsForAlgorithms\002.png";
            AES aes = new AES();
            
            Bitmap mutable = new Bitmap(Path);
            Bitmap Image = null;

            byte[] decodedText = Encoding.Unicode.GetBytes(StringText);
            byte[] Text = aes.Encrypt(decodedText);
            LSB lsbH = new LSB(mutable, Text, Path);
            if (lsbH.GetVerdict(StringText, mutable) == true)
            {
                Image = lsbH.HideLSB();
            }
            //Extract
            LSB lsdE = new LSB(Image);
            byte[] data = lsdE.ExtractData();
            string newText = Encoding.Unicode.GetString(aes.Decrypt(data));
            Assert.AreEqual(StringText, newText);
        }
        [TestMethod]
        public void TestLSBwithSpetialPhoto()
        {
            string StringText = "Возможность нахождения поисковых ключей в тексте и определения их количества полезна как для написания нового текста, так и для оптимизации уже существующего. Расположение ключевых слов по группам и по " +
                          "частоте сделает навигацию по ключам удобной и быстрой. Сервис также найдет и морфологические вашианты ключей, которые выделятся в тексте при нажатии на нужное ключевое слово. Определение процента водности " +
                          "текста When answering a question please: Read the question carefully. Understand that English isn't everyone's first language so be lenient of bad spelling and grammar. If a question is poorly phrased then " +
                          "either ask for clarification, ignore it, or edit the question and fix the problem.Insults are not welcome. Dont tell someone to read the manual. Chances are they have and dont get it.Provide an answer or move " +
                          "on to the next question.";
            string Path = @"C:\Users\Mr. Igor\source\repos\steganography\TestsForAlgorithms\002.png";
            const int Salt = 1243718;
            AES aes = new AES();

            Bitmap mutable = new Bitmap(Path);
            Bitmap Image = mutable;
            byte[] byteText = Encoding.Unicode.GetBytes(StringText);
            byte[] Text = aes.Encrypt(byteText);
            //Hide
            LSB_with_photo_library With_lib_H = new LSB_with_photo_library(Text, Salt, Image);
            Image = With_lib_H.Hide();


            //Podgotovka
            LSB_with_photo_library lsb = new LSB_with_photo_library();
            int countPixels = lsb.ExtractBitesHidenMessage(Image);
            bool Invert = lsb.ExtractBitInvert(Image);

            byte[] Bytes_ImagewithHidenData = lsb.CutImage(Image, countPixels);
            byte[] Bytes_ImageSource = lsb.CutImage(mutable, countPixels);

            //Extract
            LSB_with_photo_library With_lib_E = new LSB_with_photo_library(Salt,
                    Bytes_ImageSource, Bytes_ImagewithHidenData);

            string newText = Encoding.Unicode.GetString(aes.Decrypt(With_lib_E.Extract(countPixels, Invert)));
            Assert.AreEqual(0, 0);
        }
        [TestMethod]
        public void TestKochZao()
        {
            //Hide
            string StringText = "Возможность нахождения поисковых ключей в тексте и определения их количества полезна как для написания нового текста, так и для оптимизации уже существующего. Расположение ключевых слов по группам и по " +
                          "частоте сделает навигацию по ключам удобной и быстрой. Сервис также найдет и морфологические вашианты ключей, которые выделятся в тексте при нажатии на нужное ключевое слово. Определение процента водности " +
                          "текста When answering a question please: Read the question carefully. Understand that English isn't everyone's first language so be lenient of bad spelling and grammar. If a question is poorly phrased then " +
                          "either ask for clarification, ignore it, or edit the question and fix the problem.Insults are not welcome. Dont tell someone to read the manual. Chances are they have and dont get it.Provide an answer or move " +
                          "on to the next question.";
            string Path = @"C:\Users\Mr. Igor\source\repos\steganography\TestsForAlgorithms\001.jpg";
            AES aes = new AES();

            Bitmap mutable = new Bitmap(Path);
            Bitmap Image = null;

            Koch_Zhao kochH = new Koch_Zhao(mutable);
            byte[] Text = Coder.Coding(Encoding.Unicode.GetBytes(StringText));
            if (kochH.ValidText(Text))
            {
                Image = kochH.Hide(Text);
            }

            Koch_Zhao kochE = new Koch_Zhao(mutable);
            byte[] text = Coder.Decoding(kochE.Extract(mutable));
            string newText = Encoding.Unicode.GetString(text);
            Assert.AreEqual(StringText, newText);
        }
    }
}
