﻿using Android.Graphics;
using Newtonsoft.Json;
using Plugin.Media;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace steganography
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Concealment : ContentPage//ContentView
    {
		public Concealment()
		{
			InitializeComponent();
		}
        public string resource;
        private string ChosenAlgorithm;

        private async void Work_Image_Clicked(object sender, EventArgs e){
            if (!CrossMedia.Current.IsPickPhotoSupported){
                await DisplayAlert("Photos Not Supported", ":( Permission not granted to photos.", "OK");
                return;
            }
            var file = await Plugin.Media.CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions{
                PhotoSize = Plugin.Media.Abstractions.PhotoSize.Full,
            });
            
            if (file == null)
                return;

            Work_Image.Source = ImageSource.FromStream(() =>{
                var stream = file.GetStream();
                resource = file.Path;
                file.Dispose();
                return stream;
            });
        }

        private void Encode_Click(object sender, EventArgs e)
        {
            if ((ChosenAlgorithm == null) || (ChosenAlgorithm == ""))
            {
                DisplayAlert("!!!", "Выберите стегоалгоритм!", "OK!");
                return;
            }
            try
            {
                var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                var filename = System.IO.Path.Combine(documents, "DefaultData.json");
                var readData = File.ReadAllText(filename);
                SavedData appData = JsonConvert.DeserializeObject<SavedData>(readData);

                byte[] la = File.ReadAllBytes(resource);
                Bitmap mutable = BitmapFactory.DecodeByteArray(la, 0, la.Length).Copy(Bitmap.Config.Argb8888, true);

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                switch (ChosenAlgorithm)
                {
                    case "LSB":
                        {
                            AES aes = new AES(Methods.Out16Rich(appData.KeyAES), Methods.Out16Rich(appData.IVAES));

                            byte[] decodedText = Encoding.Unicode.GetBytes(Editor_text.Text);
                            byte[] Text = aes.Encrypt(decodedText);//Кодирование текста согласно Unicode в массив байт
                            //byte[] Text = Encoding.Unicode.GetBytes(Editor_text.Text);//Кодирование текста согласно Unicode в массив байт

                            LSB lsb = new LSB(mutable, Text, resource);
                            if (lsb.GetVerdict(Editor_text.Text, mutable) == true)
                            {
                                lsb.HideLSB();
                            }
                            else
                                DisplayAlert("!!!", "Скрываемый текст слишком велик, введите меньший объем текста!", "OK!");
                            mutable.Dispose();
                            break;
                        }
                    case "LSB with photo library":
                        {
                            AES aes = new AES(Methods.Out16Rich(appData.KeyAES), Methods.Out16Rich(appData.IVAES));
                            byte[] byteText = Encoding.Unicode.GetBytes(Editor_text.Text);
                            byte[] Text = aes.Encrypt(byteText);
                            //byte[] Text = Encoding.Unicode.GetBytes(Editor_text.Text);
                            LSB_with_photo_library With_lib = new LSB_with_photo_library(Text, Math.Abs(Convert.ToInt32(appData.Salt)), mutable, resource);

                            if (With_lib.ValidText(mutable.Height * mutable.Width, With_lib.RandomArray.Length + (5 * 8)))
                            {
                                With_lib.Hide();
                            }
                            else
                                DisplayAlert("!!!", "Скрываемый текст слишком велик, введите меньший объем текста!", "OK!");
                            mutable.Dispose();
                            break;
                        }
                    case "Koch-Zhao":
                        {
                            Koch_Zhao koch = new Koch_Zhao(mutable);
                            byte[] Text = Coder.Coding(Encoding.Unicode.GetBytes(Editor_text.Text));
                            if (koch.ValidText(Text))
                            {
                                Bitmap Image = koch.Hide(Text);
                                koch.Save(resource, Image);

                                mutable.Dispose();
                                Image.Dispose();
                            }
                            else
                            {
                                DisplayAlert("!!!", "Исходный текст слишком велик - " + Editor_text.Text.Length + "\nМаксимальное к-во символов - " + 
                                    (koch.Nc * 32) + "\n Необходимо убрать " + (Editor_text.Text.Length - (koch.Nc * 32)) + " символов", "OK!");
                                stopWatch.Stop();
                                return;
                            }
                            break;
                        }
                }
                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                    ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
                DisplayAlert("!!!", "Процесс успешно завершен!\n При этом потребовалось: " + elapsedTime, "OK!");
            }
            catch
            {
                DisplayAlert("!!!", "В настройка выберите подходящий ключ шифрования и вектор " +
                    "инициализации, после чего следует сохранить настройки!", "OK!");
            }
        }

        void SelectedItemsAlgorithm(object sender, EventArgs e)
        {
            ChosenAlgorithm = Picker_algorithms.Items[Picker_algorithms.SelectedIndex];
            Picker_algorithms.Title = "Chosen: " + ChosenAlgorithm;
            DisplayAlert("!!!", "Вы выбрали: " + ChosenAlgorithm, "OK!");
        }
        
    }
}