﻿using Android.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace steganography
{
    public class Koch_Zhao
    {
        // координаты коэффициетов , которые меняем
        public int v1 { get; set; }
        public int v2 { get; set; }
        public int u1 { get; set; }
        public int u2 { get; set; }
        //порог изменения коэффициентов
        public int P { get; set; }

        private int N;
        /// <summary>
        /// общеe число сегментов
        /// </summary>
        public int Nc { get; private set; }
        private int x;
        private int y;
        private Bitmap bmp;
        private byte[] allbytes;

        public Koch_Zhao(Bitmap bmp)
        {
            u1 = 3; v1 = 4; u2 = 4; v2 = 3;
            P = 25;
            this.bmp = bmp;
            y = ((bmp.Height % 8) != 0) ? bmp.Height - (bmp.Height % 8) : bmp.Height;
            x = ((bmp.Width % 8) != 0) ? bmp.Width - (bmp.Width % 8) : bmp.Width;
            N = 8; //размерность сегментов
            Nc = x * y / (N * N);
        }

        /// <summary>
        /// Сокрытие данных
        /// </summary>
        /// <param name="text"></param>
        /// <param name="bmp"></param>
        /// <returns></returns>
        public Bitmap Hide(byte[] text)
        {
            allbytes = text;
            byte[] endMarker = { 0x4c, 0x53, 0x42 };//Массив маркера конца скрываемой информации
            Array.Resize(ref allbytes, allbytes.Length + 3);// Измените размер массива сообщений с учетом конечного маркера
            Array.Copy(endMarker, 0, allbytes, allbytes.Length - 3, 3);//установление маркера в сконце скрываемого текста

            Byte[,] B = new Byte[x, y];
            Byte[,] R = new Byte[x, y];
            Byte[,] G = new Byte[x, y];
            //выбираем из картинки синюю компоненту RGB-модели
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                {
                    int pixel = bmp.GetPixel(i, j);
                    B[i, j] = Convert.ToByte(Color.GetBlueComponent(pixel)); // массив значений синей компоненты исходного изображения
                    R[i, j] = Convert.ToByte(Color.GetRedComponent(pixel));
                    G[i, j] = Convert.ToByte(Color.GetGreenComponent(pixel));
                }

            List<byte[,]> C = new List<byte[,]>(); //разбиваем массив В на матрицы 8 на 8

            //разбиваем массив B на сегменты С
            int C1 = 0, C2 = N - 1;
            int r1, r2;
            for (int b = 0; b < Nc; b++)
            {
                r1 = N * b % x;
                r2 = r1 + N - 1;
                C.Add(submatrix(B, r1, r2, C1, C2));
                if (r2 == x - 1)
                {
                    C1 += N;
                    C2 += N;
                }
            }
            List<double[,]> dkp_matrix = new List<double[,]>(); //список матриц 8на8 коэффициентов дкп
            for (int i = 0; i < Nc; i++)
                dkp_matrix.Add(dkp(C[i]));

            int a = 0;

            //Меняем коэффициенты ДКП в матрицах 8на8 и записываем в список матриц
            for (int m = 0; m < allbytes.Length; m++)
            {
                Bits bits = new Bits(allbytes[m]);
                for (int i = 0; i < bits.Length; i++)
                    dkp_matrix[a++] = dkp_coeffs_change(dkp_matrix[a], bits[i], u1, v1, u2, v2, P);
            }
            //обратное дкп, возвращает измененный лист сегментов 8на8
            List<double[,]> C_new = new List<double[,]>();
            for (int i = 0; i < dkp_matrix.Count; i++)
                C_new.Add(odkp(dkp_matrix[i]));

            //собираем сегменты в массив
            double[,] B1 = new double[x, y];
            r1 = -N; r2 = -1; C1 = 0; C2 = N - 1;
            for (int b = 0; b < Nc; b++)
            {
                r1 += N;
                r2 += N;
                B1 = unionmatrix(B1, C_new[b], r1, r2, C1, C2);
                if (r2 == x - 1)
                {
                    r1 = -N;
                    r2 = -1;
                    C1 += N;
                    C2 += N;
                }
            }
            B1 = norm(B1);
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                    bmp.SetPixel(i, j, new Color(R[i, j], G[i, j], (byte)Math.Round(B1[i, j])));

            return bmp;
        }
        /// <summary>
        /// Если текст помещаеться в блоки то true
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool ValidText(byte[] text)
        {
            return (((text.Length * 8) + 24) < Nc) ? true : false;
        }
        /// <summary>
        /// Нормализация массива
        /// </summary>
        /// <param name="one"></param>
        /// <returns></returns>
        static double[,] norm(double[,] one)
        {
            double min = Double.MaxValue, max = Double.MinValue;

            for (int i = 0; i < one.GetLength(0); i++)
                for (int j = 0; j < one.GetLength(1); j++)
                {
                    if (one[i, j] > max) max = one[i, j];
                    if (one[i, j] < min) min = one[i, j];
                }

            double[,] two = new double[one.GetLength(0), one.GetLength(1)];
            for (int i = 0; i < one.GetLength(0); i++)
                for (int j = 0; j < one.GetLength(1); j++)
                    two[i, j] = 255 * (one[i, j] + Math.Abs(min)) / (max + Math.Abs(min));

            return two;
        }
        /// <summary>
        /// матрица объединения
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        static double[,] unionmatrix(double[,] one, double[,] two, int a, int b, int c, int d)
        {
            for (int i = a, k = 0; i <= b; i++, k++)
                for (int j = c, l = 0; j <= d; j++, l++)
                    one[i, j] = two[k, l];
            return one;
        }
        /// <summary>
        /// Создание копии двумерного массива
        /// </summary>
        /// <param name="one"></param>
        /// <returns></returns>
        static double[,] teta(double[,] one)
        {
            double[,] two = new double[one.GetLength(0), one.GetLength(1)];
            for (int i = 0; i < one.GetLength(0); i++)
                for (int j = 0; j < one.GetLength(1); j++)
                    two[i, j] = one[i, j];
            return two;
        }
        /// <summary>
        /// Извлечение данных
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        public byte[] Extract(Bitmap bmp)
        {
            Byte[,] B = new Byte[x, y];
            Byte[,] R = new Byte[x, y];
            Byte[,] G = new Byte[x, y];
            //выбираем из картинки синюю компоненту RGB-модели
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                {
                    int pixel = bmp.GetPixel(i, j);
                    B[i, j] = Convert.ToByte(Color.GetBlueComponent(pixel)); // массив значений синей компоненты исходного изображения
                    R[i, j] = Convert.ToByte(Color.GetRedComponent(pixel));
                    G[i, j] = Convert.ToByte(Color.GetGreenComponent(pixel));
                }

            List<byte[,]> C = new List<byte[,]>(); //разбиваем массив В на матрицы 8 на 8           
            //разбиваем массив B на сегменты С
            int C1 = 0, C2 = N - 1;
            int r1, r2;
            for (int b = 0; b < Nc; b++)
            {
                r1 = N * b % x;
                r2 = r1 + N - 1;
                C.Add(submatrix(B, r1, r2, C1, C2));
                if (r2 == x - 1)
                {
                    C1 += N;
                    C2 += N;
                }
            }

            List<double[,]> dkp_matrix = new List<double[,]>(); //список матриц 8на8 коэффициентов дкп
            for (int i = 0; i < Nc; i++)
                dkp_matrix.Add(dkp(C[i]));

            double[,] dkp_8x8; //матрица 8на8 для коэффициентов ДКП
            double Abs1, Abs2;
            string bits = "";
            List<byte> bytetext = new List<byte>();
            char[] cc = new char[dkp_matrix.Count / 8];

            for (int k = 0; k < Nc; k++)
            {
                dkp_8x8 = teta(dkp_matrix[k]);
                Abs1 = Math.Abs(dkp_8x8[u1, v1]);
                Abs2 = Math.Abs(dkp_8x8[u2, v2]);
                if (Abs1 > Abs2)
                    bits += "0";
                if (Abs1 < Abs2)
                    bits += "1";
                if (bits.Length == 8)
                {
                    bytetext.Add(Convert.ToByte(new Bits(bits).Number));
                    bits = "";

                    if (bytetext.Count >= 3)
                        if (Cut(bytetext, bytetext.Count))
                        {
                            bytetext.RemoveAt(bytetext.Count - 1);
                            bytetext.RemoveAt(bytetext.Count - 1);
                            bytetext.RemoveAt(bytetext.Count - 1);
                            return bytetext.ToArray();
                        }
                }
            }
            ;
            return bytetext.ToArray();
        }

        /// <summary>
        /// Извлечение маркера и лишних символов из байтового массива
        /// </summary>
        /// <param name="outputData"></param>
        /// <returns></returns>
        public bool Cut(List<byte> outputData, int startindex)
        {
            for (int i = startindex; i <= outputData.Count; i++)
            {
                if (outputData[i - 1] == 0x42 && outputData[i - 2] == 0x53 && outputData[i - 3] == 0x4C)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Выбор коефициентов ДКП
        /// </summary>
        /// <param name="one"></param>
        /// <param name="i"></param>
        /// <param name="u1"></param>
        /// <param name="v1"></param>
        /// <param name="u2"></param>
        /// <param name="v2"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        double[,] dkp_coeffs_change(double[,] one, int i, int u1, int v1, int u2, int v2, int P)
        {
            var temp = (one);

            double Abs1, Abs2;
            double z1 = 0, z2 = 0;
            Abs1 = Math.Abs(temp[u1, v1]);
            Abs2 = Math.Abs(temp[u2, v2]);
            if (temp[u1, v1] >= 0) z1 = 1;
            else z1 = -1;
            if (temp[u2, v2] >= 0) z2 = 1;
            else z2 = -1;

            if (i == 0)
            {
                if (Abs1 - Abs2 <= P)
                    Abs1 = P + Abs2 + 1;
            }
            if (i == 1)
            {
                if (Abs1 - Abs2 >= -P)
                    Abs2 = P + Abs1 + 1;
            }
            temp[u1, v1] = z1 * Abs1;
            temp[u2, v2] = z2 * Abs2;
            return temp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <returns></returns>
        byte[,] stack(byte[,] one, byte[,] two)
        {
            byte[,] three = new byte[one.GetLength(0) + two.GetLength(0), one.GetLength(1)];
            for (int i = 0; i < one.GetLength(0); i++)
                for (int j = 0; j < one.GetLength(1); j++)
                {
                    three[i, j] = one[i, j];
                }
            for (int i = one.GetLength(0); i < three.GetLength(0); i++)
                for (int j = 0; j < 8; j++)
                {
                    three[i, j] = two[i - one.GetLength(0), j];
                }
            return three;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="one"></param>
        /// <param name="two"></param>
        /// <returns></returns>
        byte[,] augment(byte[,] one, byte[,] two)
        {
            byte[,] three = new byte[one.GetLength(0), one.GetLength(1) + two.GetLength(1)];
            for (int i = 0; i < one.GetLength(0); i++)
                for (int j = 0; j < one.GetLength(1); j++)
                {
                    three[i, j] = one[i, j];
                }
            for (int i = 0; i < three.GetLength(0); i++)
                for (int j = one.GetLength(1); j < three.GetLength(1); j++)
                {
                    three[i, j] = two[i, j - one.GetLength(1)];
                }
            return three;
        }
        /// <summary>
        /// Создание подматрицы
        /// </summary>
        /// <param name="one"></param>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        static byte[,] submatrix(byte[,] one, int a, int b, int c, int d)
        {
            byte[,] temp = new byte[b - a + 1, d - c + 1];
            for (int i = a, k = 0; i <= b; i++, k++)
                for (int j = c, l = 0; j <= d; j++, l++)
                    temp[k, l] = one[i, j];

            return temp;
        }
        /// <summary>
        /// ДКП
        /// </summary>
        /// <param name="one"></param>
        /// <returns></returns>
        static double[,] dkp(byte[,] one)
        {
            int n = one.GetLength(0);
            double[,] two = new double[n, n];
            double U, V, temp = 0;
            for (int v = 0; v < n; v++)
            {
                for (int u = 0; u < n; u++)
                {
                    if (v == 0) V = 1.0 / Math.Sqrt(2);
                    else V = 1;
                    if (u == 0) U = 1.0 / Math.Sqrt(2);
                    else U = 1;
                    temp = 0;
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < n; j++)
                        {
                            temp += one[i, j] * Math.Cos(Math.PI * v * (2 * i + 1) / (2 * n)) *
                                Math.Cos(Math.PI * u * (2 * j + 1) / (2 * n));
                        }
                    }
                    two[v, u] = U * V * temp / (Math.Sqrt(2 * n));
                }
            }
            return two;
        }
        /// <summary>
        /// Обратное ДКП
        /// </summary>
        /// <param name="one"></param>
        /// <returns></returns>
        static double[,] odkp(double[,] one)
        {
            int n = one.GetLength(0);
            double[,] two = new double[n, n];
            double U, V, temp = 0;
            for (int v = 0; v < n; v++)
            {
                for (int u = 0; u < n; u++)
                {
                    temp = 0;
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < n; j++)
                        {
                            if (i == 0) V = 1.0 / Math.Sqrt(2);
                            else V = 1;
                            if (j == 0) U = 1.0 / Math.Sqrt(2);
                            else U = 1;
                            temp += U * V * one[i, j] * Math.Cos(Math.PI * i * (2 * v + 1) / (2 * n)) *
                                Math.Cos(Math.PI * j * (2 * u + 1) / (2 * n));
                        }
                    }
                    two[v, u] = temp / (Math.Sqrt(2 * n));
                }
            }
            return two;
        }
        /// <summary>
        /// Save Image
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="Image"></param>
        public void Save(string Path, Bitmap Image)
        {
            var sdCardPath = Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            string s = Path.Remove(0, Path.LastIndexOf("/") + 1);
            var filePath = System.IO.Path.Combine(sdCardPath, s);
            var stream = new FileStream(filePath, FileMode.Create);
            Image.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
            stream.Close();
        }
    }
}
