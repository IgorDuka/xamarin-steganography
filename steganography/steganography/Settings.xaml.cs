﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace steganography
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Settings : ContentPage
	{
		public Settings ()
		{
			InitializeComponent ();

            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var filename = Path.Combine(documents, "DefaultData.json");
            try
            {
                var readData = File.ReadAllText(filename);

                AES aes = new AES();

                SavedData appData = JsonConvert.DeserializeObject<SavedData>(readData);
                if ((appData.IVAES != null) && (appData.IVAES != ""))
                    Enter_AESIV.Text = appData.IVAES;
                else
                {
                    Enter_AESIV.Text = Methods.Creat16Rich(aes.IV);
                }
                if ((appData.KeyAES != null) && (appData.KeyAES != ""))
                    Enter_AESkey.Text = appData.KeyAES;
                else
                {
                    Enter_AESkey.Text = Methods.Creat16Rich(aes.Key);
                }
                if ((appData.Salt != null) && (appData.Salt != ""))
                    Enter_Salt.Text = appData.Salt;
                else
                {
                    Random rnd = new Random();
                    Enter_Salt.Text = rnd.Next(Int32.MinValue, Int32.MaxValue).ToString();
                }
            }
            catch{
                AES aes = new AES();

                Enter_AESIV.Text = Methods.Creat16Rich(aes.IV);
                Enter_AESkey.Text = Methods.Creat16Rich(aes.Key);
                Random rnd = new Random();
                Enter_Salt.Text = rnd.Next(Int32.MinValue, Int32.MaxValue).ToString();
            }
        }
        
        private void Save_Settings_Click(object sender, EventArgs e)
        {
            SavedData save = new SavedData(Enter_AESkey.Text, Enter_AESIV.Text, Enter_Salt.Text);
            var Data = JsonConvert.SerializeObject(save);

            var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var filename = Path.Combine(documents, "DefaultData.json");
            File.WriteAllText(filename, Data);
        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            AES aes = new AES();
            Enter_AESIV.Text = Methods.Creat16Rich(aes.IV);
            Enter_AESkey.Text = Methods.Creat16Rich(aes.Key);
            Random rnd = new Random();
            Enter_Salt.Text = rnd.Next(Int32.MinValue, Int32.MaxValue).ToString();
        }
    }
}