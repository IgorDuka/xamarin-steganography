﻿namespace steganography
{
    public static class Coder
    {
        /// <summary>
        /// Кодирование сообщения инверсным кодом с избыточностью "в 2 раза"
        /// </summary>
        /// <param name="message">сообщение подвергаемое кодированию</param>
        /// <returns></returns>
        static public byte[] Coding(byte[] message)
        {
            byte[] coded = new byte[message.Length * 2];
            byte[] OneBits = CutByteOnbit(message);
            for (int i = 0, k = 0; k < message.Length; i += 2, k++)
            {
                coded[i] = message[k];
                int One = 0;
                for (int j = 0; j < 8; j++)//Подсчёт к-ва единиц в байте
                    if (OneBits[k * 8 + j] == (byte)(0x1))
                        One++;
                if ((One % 2) != 0)//Инвертирование если (((к-во 1) % 2) != 0)
                    coded[i + 1] = (byte)(~message[k]);
                else if ((One % 2) == 0)
                    coded[i + 1] = message[k];
            }
            return coded;
        }

        /// <summary>
        /// Декодирование сообщения инверсным кодом и исправление возможных ошибок если таковы имеються
        /// </summary>
        /// <param name="message">сообщение подвергаемое декодированию</param>
        /// <returns></returns>
        static public byte[] Decoding(byte[] message)
        {
            byte[] decoded = new byte[message.Length / 2];
            byte[] OneBits = CutByteOnbit(message);

            for (int i = 1, k = 0; k < decoded.Length; i += 2, k++)
            {
                //приведение проверочных битов к инверсной или не инверсной форме в зависимости от исходных битов
                int One = 0;
                for (int j = 0; j < 8; j++)//Подсчёт к-ва единиц в байте
                    if (OneBits[i * 8 + j] == (byte)(0x1))
                        One++;
                if ((One % 2) != 0)//Инвертирование если (((к-во 1) % 2) != 0)
                    decoded[k] = (byte)(~message[i]);
                else if ((One % 2) == 0)
                    decoded[k] = message[i];
                decoded[k] = (byte)(message[i] ^ decoded[k]);//получение кода ошибок в исодном сообщении
                decoded[k] = (byte)(message[i] ^ decoded[k]);//исправление ошибок
            }
            return decoded;
        }

        /// <summary>
        /// Разбиение 8-битового значение на 8 ячеек по 1 символу в том же порядке
        /// </summary>
        /// <param name="Array"></param>
        /// <returns></returns>
        static public byte[] CutByteOnbit(byte[] Array)
        {
            byte[] OneArray = new byte[Array.Length * 8];
            for (int i = 0, symbols = 0; i < (Array.Length * 8); i += 8, symbols++)
            {
                for (int j = 0, shift = 7; j < 8; j++, shift--)
                    OneArray[i + j] = (byte)((Array[symbols] >> shift) & 0x0000_0001);
            }
            return OneArray;
        }
    }
}
